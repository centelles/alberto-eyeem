# Game of pairs #

### The task ###

● get a bunch of photos from the EyeEm API

● display the game cards (photos "facing down") at random positions

● each photo needs to have a match somewhere else on the field

● players can click on cards to reveal the underlying photo

● they can reveal two photos per round

● finding a pair of matching cards gives one point and an extra turn

● not finding a pair results in the next player's turn

● keep track of the score during the game

● when there are no cards left the player with the highest score wins


### Bonus ###

● multiplayer support

● animations

● nice layout


### Conditions ###

● use react.js

● no database should be needed

● software doesn’t need to have tests

● setup should be done by running "npm install“

● the result should be sent in as a git repository (github/bitbucket) or a zipped folder via mail


### Set up ###

```
npm install
npm start
```

The game will be listening at localhost:8080